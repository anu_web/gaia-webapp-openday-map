const path = require('path');
const gulp = require('gulp');
const glob = require('glob');
const through = require('through2');
const ejs = require('gulp-ejs');
const rename = require('gulp-rename');
const clean = require('gulp-clean');

gulp.task('build', ['build-dist']);
gulp.task('clean', ['clean-dist']);

gulp.task('build-dist', ['dist-appinfo', 'dist-gaia-webapp-lib']);

gulp.task('dist-files', ['clean-dist'], function () {
  return gulp.src(['./src/*.json', './umd/*.min.js', './umd/*.*.css'])
    .pipe(gulp.dest('./dist'));
});

gulp.task('dist-appinfo', ['dist-files'], function () {
  // Process dist/app.json
  return gulp.src('./dist/app.json')
    .pipe(through.obj(function (file, enc, cb) {
      return parseJsonPromise(file, enc)
        .then((data) => {
          // Set attribute from files.
          Object.keys(data).forEach((key) => {
            const value = data[key];

            // Parse property name.
            if (typeof value === 'string' && value.length > 2
                && value[0] === '[' && value[value.length - 1] === ']') {
              const sepIndex = value.indexOf(':');
              if (sepIndex > 0) {
                const filename = value.substring(1, sepIndex);
                const prop = value.substring(sepIndex + 1, value.length - 1);

                // Load file and get value.
                if (filename.endsWith('.js') || filename.endsWith('.json')) {
                  let propvalue = require(path.resolve(__dirname, filename));
                  prop.split('.').forEach((subprop) => {
                    if (typeof(propvalue) === 'object' && propvalue.hasOwnProperty(subprop)) {
                      propvalue = propvalue[subprop];
                    } else {
                      throw new Error(`Property '${prop}' not found in '${filename}`);
                    }
                  });

                  data[key] = propvalue;
                }
              }
            }

            // TODO Process deep values.
          });

          return data;
        })
        .then((data) => Promise.resolve({})
          .then((assets) => new Promise((resolve) => {
            glob('dist/*.*.css', function (er, files) {
              assets['css'] = files;
              resolve(assets);
            });
          }))
          .then((assets) => new Promise((resolve) => {
            glob('dist/*.min.js', function (er, files) {
              assets['js'] = files;
              resolve(assets);
            });
          }))
          .then((assets) => {
            data.assets = assets;
            return data;
          })
        )
        .then((data) => JSON.stringify(data, null, 2))
        .then((json) => {
          // Finish processing.
          file.contents = new Buffer(json);
          this.push(file);
          cb();
        })
        .catch((e) => {
          this.emit('error', e);
          this.emit('end');
        });
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('dist-gaia-webapp-lib', ['dist-appinfo'], function () {
  // Generate dist/[project].libraries.info
  return gulp.src('./dist/app.json')
    .pipe(through.obj(function (file, enc, cb) {
      return parseJsonPromise(file, enc).then((data) => gulp.src('./src/app.libraries.info.ejs')
        .pipe(ejs(data))
        .pipe(rename(data['name'] + '.libraries.info'))
        .pipe(gulp.dest('./dist'))
      ).then(() => { cb() });
    }));
});

gulp.task('clean-dist', function () {
  return gulp.src('./dist/*', {read: false})
    .pipe(clean());
});

function parseJsonPromise(file, enc) {
  return new Promise((resolve, reject) => {
    // Parse JSON file.
    try {
      if (file.isBuffer()) {
        let fileContent = file.contents.toString(enc);
        let data = JSON.parse(fileContent);
        resolve(data);
      } else {
        reject(new Error('Not a buffer'));
      }
    } catch (e) {
      reject(e);
    }
  })
}
