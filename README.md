# gaia-webapp-boilerplate-react

Gaia web app boilerplate for React

## Getting started

1. Fork repository.
2. Implement project, starting with:
   * `package.json`: update project details in `name`, `version`, `description`, `repository`
   * `nwb.config.js`: update exported library (global) name in `npm.umd.global`
   * `src/`: application component code
   * `tests/`: automated tests
   * `demo/src/`: demo code
3. Update this README.

## Deploying web app

After building, the `dist` directory contains everything necessary for
deployment as a frontend app.

For example, the app can be installed in Drupal as a library under
`sites/all/libraries/my-app/*` that contains every file under `dist`.
