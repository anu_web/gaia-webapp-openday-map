import React from 'react'
import {render} from 'react-dom'
import {App} from './app'

export default function (bootstrapConfig) {
  const {settings, element} = bootstrapConfig;
  if (element === undefined) {
    throw new Error('Document element is required for this bootstrap');
  }
  render(<App {...(settings || {})}/>, element);
}
