module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'gaia_webapp_boilerplate_react',
      externals: {
        react: 'React'
      }
    }
  }
}
